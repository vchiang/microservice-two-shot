from django.urls import path
from . import views


urlpatterns = [
    path('hats/', views.api_list_hats, name='hat_list'),
    path('hats/<int:pk>/', views.api_show_hats, name="hat_show"),

]

from django.shortcuts import render
from django.http import JsonResponse,HttpResponse
from .models import Hat
from common.json import ModelEncoder
import json

from.models import Hat,LocationVO
from django.views.decorators.http import require_http_methods
# Create your views here.

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties= [
        "import_href",
        "closet_name"
    ]




class HatListEncoder(ModelEncoder):
    model=Hat
    properties=[
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id",
        "location",
    ]
    encoders={
        "location":LocationVOEncoder()
    }


@require_http_methods(["GET","POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method=="GET":
        if location_vo_id==None:
            hats=Hat.objects.all()
            return JsonResponse(
                {"hats":hats},
                encoder=HatListEncoder,
            )
        else:
            hats=Hat.objects.filter(location=location_vo_id)
            return JsonResponse(
                {"hats":hats},
                encoder=HatListEncoder,
            )
    else:
        content=json.loads(request.body)
        print(content)




        try:
            location_href=content["location"]
            location=LocationVO.objects.get(import_href=location_href)
            content["location"]=location
            print(content)
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {'message': "Invalid location id"},
                status=400,
            )
        hat=Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE","GET","PUT"])
def api_show_hats(requst,pk):
    if requst.method =="GET":
        hat=Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )
    elif requst.method == "DELETE":
        count,_=Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted":count>0})
    else:
        content=json.loads(requst.body)
        try:
            if "location" in content:
                location=LocationVO.objects.get(location=content["location"])
                content["location"]=location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )
        Hat.objects.filter(id=pk).update(**content)
        hat=Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )

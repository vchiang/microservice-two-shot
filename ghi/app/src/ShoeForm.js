import React, { useState, useEffect } from "react";

function ShoeForm(props) {
  const [manufacturer, setManufacturer] = useState("");
  const [modelName, setModelName] = useState("");
  const [color, setColor] = useState("");
  const [pictureURL, setPictureUrl] = useState("");
  const [wardrobeBin, setWardrobeBin] = useState("");
  const [binList, setBinList] = useState([]);

  const fetchBins = async () => {
    const url = "http://localhost:8100/api/bins/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data.bins);
      setBinList(data.bins);
    } else {
      console.error("An error ocured fetching bin data");
    }
  };

  useEffect(() => {
    fetchBins();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      manufacturer: manufacturer,
      model_name: modelName,
      color: color,
      picture_url: pictureURL,
      wardrobe_bin: wardrobeBin,
    };

    console.log(data);

    const shoeUrl = "http://localhost:8080/shoes/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(shoeUrl, fetchConfig);
    console.log(response);

    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);

      setManufacturer("");
      setModelName("");
      setColor("");
      setPictureUrl("");
      setWardrobeBin("");
      //   console.log(newShoe);
    } else {
      console.log("An error occured fetching the data");
    }
  };

  function handleManufacturerChange(event) {
    const { value } = event.target;
    setManufacturer(value);
  }

  function handleModelNameChange(event) {
    const { value } = event.target;
    setModelName(value);
  }

  function handleColorChange(event) {
    const { value } = event.target;
    setColor(value);
  }

  function handlePictureUrlChange(event) {
    const { value } = event.target;
    setPictureUrl(value);
  }

  function handleWardrobeBinChange(event) {
    const { value } = event.target;
    setWardrobeBin(value);
  }

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create New Shoe</h1>
              <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={handleManufacturerChange}
                    placeholder="Manufacturer"
                    required
                    type="text"
                    name="manufacturer"
                    id="presenter_name"
                    className="form-control"
                    value={manufacturer}
                  />
                  <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={handleModelNameChange}
                    placeholder="Model name"
                    required
                    type="text"
                    name="model_name"
                    id="model_name"
                    className="form-control"
                    value={modelName}
                  />
                  <label htmlFor="model_name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={handleColorChange}
                    placeholder="Color"
                    required
                    type="text"
                    name="color"
                    id="color"
                    className="form-control"
                    value={color}
                  />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={handlePictureUrlChange}
                    placeholder="Picture URL"
                    required
                    type="url"
                    name="picture_url"
                    id="picture_url"
                    className="form-control"
                    value={pictureURL}
                  />
                  <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="form-floating mb-3">
                  <select
                    onChange={handleWardrobeBinChange}
                    placeholder="Wardrobe Bin"
                    required
                    className="form-select"
                    // name="wardrobe_bin"
                    id="wardrobe_bin"
                    value={wardrobeBin}
                  >
                    <option value="">Choose a Bin</option>
                    {binList.map((bin) => {
                      return (
                        <option key={bin.href} value={bin.href}>
                          {bin.closet_name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ShoeForm;

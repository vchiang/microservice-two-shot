import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import HatsList from "./hat";
import HatsForm from "./hatform";
import ShoeList from "./ShoeList";
import ShoeForm from "./ShoeForm";
import { useState, useEffect } from "react";

function App(props) {
  const [shoes, setShoes] = useState([]);

  async function getShoes() {
    const response = await fetch("http://localhost:8080/shoes/");
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    } else {
      console.error("An error occured fetching the data");
    }
  }

  useEffect(() => {
    getShoes();
  }, []);

  if (shoes === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats/" element={<HatsList getHats={props.hats} />} />
          <Route
            path="/hats/news"
            element={<HatsForm getHats={props.hats} />}
          />
          <Route path="shoes/">
            <Route
              index
              element={<ShoeList shoes={shoes} getShoes={getShoes} />}
            />
            <Route path="new/" element={<ShoeForm />} getShoes={getShoes} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";

const deleteHat = async(import_href) => {
    console.log(import_href)
    const fetchConfig = {
        method: "DELETE",
        headers: {
          'Content-Type': 'application/json'
      },
    }
    await fetch(`http://localhost:8090/hats_rest/hats/${import_href}`, fetchConfig);
    window.location.reload()
}

function HatsList({}){
    const [hats, setHats] = useState([])

    useEffect(() => {
        fetchData();
    },[]);

     const fetchData = async () => {
        const hatUrl="http://localhost:8090/hats_rest/hats/"


        const hatResponse = await fetch(hatUrl);
        if (hatResponse.ok) {
            const data=await hatResponse.json()
            setHats(data.hats)
        }
     }

     const handleDelete = async (hat) => {
      console.log(hat)
      await deleteHat(hat.import_href);
      setHats(hats.filter(h => h.import_href !== hat.import_href));
  };


    return(
        <div>
            <Link to="news/" className="btn btn-success m-2 w-100">
            Add Hats
            </Link>
        <table className = "table table-striped">
            <thead>
                <tr>
                    <th> Style Name </th>
                    <th> Color </th>
                    <th> Fabric </th>
                    <th> Picture </th>
                    <th> Location </th>
                    <th> Delete </th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key = {hat.id}>
                            <td> {hat.style_name}</td>
                            <td> {hat.color}</td>
                            <td> {hat.fabric}</td>
                            <td>
                                < img src= { hat.picture_url } alt= "" width= "100px" height= "100px"/>
                            </td>
                            <td> {hat.location.closet_name} </td>
                            <td>
                              <button type="button" onClick={()=>deleteHat(hat.id)}className='btn btn-danger'>
                                Delete
                              </button>

                            </td>
                        </tr>
                    );
                })}
            </tbody>

        </table>
    </div>
    )
}

export default HatsList;

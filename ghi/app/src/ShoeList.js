const deleteShoe = async (id, getShoes) => {
  const url = `http://localhost:8080/shoes/${id}`;
  const fetchConfig = {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
  };
  await fetch(url, fetchConfig);
  getShoes();
};

function ShoeList(props) {
  return (
    <>
      <table className="table table-striped" id="table" data-classes="table">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>ID</th>
            <th>Delete Shoe</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map((shoe) => {
            return (
              <tr key={`${shoe.model_name}-${shoe.id}`}>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.model_name}</td>
                <td>{shoe.color}</td>
                <td>{shoe.id}</td>
                <td>
                  <button
                    onClick={() => deleteShoe(shoe.id, props.getShoes)}
                    type="button"
                    className="btn btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ShoeList;

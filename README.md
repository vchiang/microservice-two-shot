# Wardrobify

Team:

- Person 1 - Anna Hou, Hats
- Person 2 - Victoria Chiang, Shoes

## Design

- first create URLS, views, and models (push to branch)

  - make model
  - create APIs & views

- merge
- create the poller

  - set up API call in insomnia
  - make dummy data?

- create react front end/SPA
  -You must create React component(s) to show a list of all shoes and their details.
  You must create React component(s) to show a form to create a new shoe.

- create delete shoe
- update existing nav links to components

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

- model: manufacturer, its model name, its color, a URL for a picture, and the bin in the wardrobe where it exists
- integration w wardrobe: ?? [to fill out alter]

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

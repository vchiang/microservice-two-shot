from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO

# Create your views here.

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "name",
        "import_href"
    ]

class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "wardrobe_bin",
        "id"
    ]

    encoders = {
        "wardrobe_bin": BinVOEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
        )
    else:
        content = json.loads(request.body)
        # print(content)
        try:
            bin_href = content["wardrobe_bin"]
            print(bin_href)
            bin = BinVO.objects.get(import_href=bin_href)
            content["wardrobe_bin"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin ID"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_shoe_detail(request, pk):
    # try:
    #     shoe = Shoe.object.get(id=pk)
    # except shoe.DoesNotExist:
    #     print("Shoe.DoesNotExist")

    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
